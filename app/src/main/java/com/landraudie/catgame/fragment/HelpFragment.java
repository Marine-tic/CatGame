package com.landraudie.catgame.fragment;

import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.landraudie.catgame.R;


public class HelpFragment extends DialogFragment {

    public static HelpFragment newInstance() {
        return new HelpFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Je reconstruis la vue
        final View helpView = inflater.inflate(R.layout.fragment_help, container, false);

        // Je rajoute les éléments de la vue
        final TextView rulesText = (TextView) helpView.findViewById(R.id.rulesText);
        final Button closeHelpButton = (Button) helpView.findViewById(R.id.closeHelpButton);

        rulesText.setText(R.string.long_help);


        closeHelpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        return helpView;
    }


}

