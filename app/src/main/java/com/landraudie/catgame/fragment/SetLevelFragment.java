package com.landraudie.catgame.fragment;

import android.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

import com.landraudie.catgame.R;
import com.landraudie.catgame.activity.GameActivity;


public class SetLevelFragment extends DialogFragment {

    public static SetLevelFragment newInstance() {
        return new SetLevelFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // I inflate the view
        View setLevelView = inflater.inflate(R.layout.fragment_setlevel, container, false);

        // I add the elements to the view
        final SeekBar seekBar = (SeekBar) setLevelView.findViewById(R.id.seekBarLevel);
        final TextView LevelText = (TextView) setLevelView.findViewById(R.id.levelText);
        final Button validateGameLevel = (Button) setLevelView.findViewById(R.id.validateGameLevel);

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int value, boolean fromUser) {
                LevelText.setText(String.valueOf(value + 1));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }

        });
        validateGameLevel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Level transmissions to the activity
                Intent intent = new Intent(getContext(), GameActivity.class);
                intent.putExtra("level", seekBar.getProgress() + 1);
                startActivity(intent);
            }
        });
        return setLevelView;
    }


}

