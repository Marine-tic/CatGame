package com.landraudie.catgame.fragment;

import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.landraudie.catgame.R;
import com.landraudie.catgame.model.Score;

import io.realm.Realm;

public class ScoreFragment extends DialogFragment {

    private Realm realm;

    public static ScoreFragment newInstance() {
        return new ScoreFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Realm.init(getContext());
        realm = Realm.getDefaultInstance();
        final View scoreView = inflater.inflate(R.layout.fragment_score, container, false);

        final TextView scoreTextView = (TextView) scoreView.findViewById(R.id.scoreText);
        final Button closeScoreButton = (Button) scoreView.findViewById(R.id.closeScoreButton);

        /*
        // get the score from shared preferences
        SharedPreferences sharedPref = this.getActivity().getSharedPreferences("myprefrences", Context.MODE_PRIVATE);
        int score = sharedPref.getInt("score", 0);
        */

        int score = 0;
        // get the score from Realm BDD
        // Create the Realm instance


        if(realm.where(Score.class).findFirst() == null) {
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    Score score = realm.createObject(Score.class);
                    score.setValeur(0);

                }
            });
        } else {
            score = realm.where(Score.class).findFirst().getValeur();
        }


        // get the score from other activities/fragment
        Bundle extras = getActivity().getIntent().getExtras();
        if (extras != null) {
            score = extras.getInt("score");
        }

        scoreTextView.setText(String.valueOf(score));


        closeScoreButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        return scoreView;
    }


}

