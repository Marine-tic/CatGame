package com.landraudie.catgame.fragment;

import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.landraudie.catgame.R;


/**
 * This class manages the About dialog
 */
public class AboutFragment extends DialogFragment {

    public static AboutFragment newInstance() {
        return new AboutFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // I inflate the viewe
        final View aboutView = inflater.inflate(R.layout.fragment_about, container, false);

        // I add elements to the view
        final TextView aboutInfo = (TextView) aboutView.findViewById(R.id.aboutInfoText);
        final Button closeAboutButton = (Button) aboutView.findViewById(R.id.closeAboutButton);

        aboutInfo.setText(R.string.long_about);


        closeAboutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        return aboutView;
    }


}

