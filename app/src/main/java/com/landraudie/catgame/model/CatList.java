package com.landraudie.catgame.model;

import java.util.ArrayList;
import java.util.List;

import com.landraudie.catgame.R;

/**
 * This class manages the list of all cats that can be used in the game.
 */
public class CatList {
    private List<Cat> catList;
    private Cat persan;
    private Cat scottish;
    private Cat mainecoon;
    private Cat sphinx;


    public CatList(){
        persan = new Cat(0,"Persan", R.drawable.persan, R.drawable.persan_small, R.id.persanButton, R.raw.persan_meow);
        scottish = new Cat(1,"Scottish", R.drawable.scottish, R.drawable.scottish_small, R.id.scottishButton, R.raw.scottish_meow);
        mainecoon = new Cat(2,"MaineCoon", R.drawable.mainecoon, R.drawable.mainecoon_small, R.id.maineCoonButton, R.raw.mainecoon_meow);
        sphinx = new Cat(3,"Sphinx", R.drawable.sphinx, R.drawable.sphinx_small, R.id.sphinxButton, R.raw.sphinx_meow);
        catList = new ArrayList<>();
        catList.add(persan);
        catList.add(scottish);
        catList.add(mainecoon);
        catList.add(sphinx);
    }
    public int size(){
        return catList.size();
    }


    public List<Cat> getCatList() {
        return catList;
    }

    public void setCatList(List<Cat> catList) {
        this.catList = catList;
    }

    public int getSizeCatList(){
        return catList.size();
    }

    public int getIndexOfCatInCatList(Cat cat){
        return catList.indexOf(cat);
    }

    public Cat getCatFromIndexInCatList(int index){
        return catList.get(index);
    }

    public Cat getCatFromNameInCatList(String name){
        for (Cat cat:catList) {
            if(cat.getName().equals(name)){
                return cat;
            }
        }
        return null;
    }
}
