package com.landraudie.catgame.model;

import io.realm.RealmObject;

public class Score extends RealmObject {

    private int valeur;

    public Score() {
        this.valeur = 0;
    }

   public Score(int valeur) {
        this.valeur = valeur;
    }

    public int getValeur() {
        return valeur;
    }

    public void setValeur(int valeur) {
        this.valeur = valeur;
    }
}
