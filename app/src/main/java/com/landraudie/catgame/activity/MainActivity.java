package com.landraudie.catgame.activity;

import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;

import com.landraudie.catgame.fragment.AboutFragment;
import com.landraudie.catgame.fragment.HelpFragment;
import com.landraudie.catgame.R;
import com.landraudie.catgame.fragment.ScoreFragment;
import com.landraudie.catgame.fragment.SetLevelFragment;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // toolbar init
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // button init
        Button button = (Button) findViewById(R.id.button);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Game Launching...", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                // Level transmission between activities
                Intent intent = new Intent(MainActivity.this, GameActivity.class);
                intent.putExtra("level", 1);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        Bundle extras = this.getIntent().getExtras();
        if (extras != null) {
            // get the score from another activityif it exists and update the shared preferences accordingly
            int score = extras.getInt("score");
            SharedPreferences sharedPref = this.getSharedPreferences("myprefrences", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putInt("score", score);
            editor.apply();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        // handle all differents dialog with each its own fragment and print it
        switch (id){
            case R.id.set_level:
                DialogFragment setLevelFragment = SetLevelFragment.newInstance();
                setLevelFragment.show(getFragmentManager(), "dialog");
            return true;
            case R.id.about:
                DialogFragment aboutFragment = AboutFragment.newInstance();
                aboutFragment.show(getFragmentManager(), "dialog");
            return true;
            case R.id.help:
                DialogFragment helpFragment = HelpFragment.newInstance();
                helpFragment.show(getFragmentManager(), "dialog");
            return true;
            case R.id.score:
                DialogFragment scoreFragment = ScoreFragment.newInstance();
                scoreFragment.show(getFragmentManager(), "dialog");
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
