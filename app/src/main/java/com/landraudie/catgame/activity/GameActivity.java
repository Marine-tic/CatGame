package com.landraudie.catgame.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import com.landraudie.catgame.R;
import com.landraudie.catgame.model.Cat;
import com.landraudie.catgame.model.CatList;
import com.landraudie.catgame.model.Score;
import io.realm.Realm;

/**
 * GameActivity is the activity where the game is with all the win/loose
 * mechanisms and score management
 */
public class GameActivity extends Activity {

    private int level;
    private int sequenceCatMaxSize;
    private CatList catList;
    private ImageButton persanImageButton;
    private ImageButton scottishImageButton;
    private ImageButton maineCoonImageButton;
    private ImageButton sphinxImageButton;

    private int score;
    private Realm realm;
    private Cat persan;
    private Cat maineCoon;
    private Cat sphinx;
    private Cat scottish;
    private List<Cat> sequenceCatComputer;
    private List<Cat> sequenceCatPlayer;

    public GameActivity() {

        // Contains the list of all possible cats
        catList = new CatList();

        // init cat object from the list
        persan = catList.getCatFromNameInCatList("Persan");
        maineCoon = catList.getCatFromNameInCatList("MaineCoon");
        sphinx = catList.getCatFromNameInCatList("Sphinx");
        scottish = catList.getCatFromNameInCatList("Scottish");

        // init cat sequence for computer and for the player
        sequenceCatComputer = new ArrayList<>();
        sequenceCatPlayer = new ArrayList<>();

    }

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // always start with the setContentView if not null pointer exception
        setContentView(R.layout.activity_game);

        // get level chosen from the main activity
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            level = extras.getInt("level");
        }
        // get the score from shared preferences
       /* SharedPreferences sharedPref = this.getSharedPreferences("myprefrences", Context.MODE_PRIVATE);
        score = sharedPref.getInt("score", 0);*/
        Realm.init(getApplicationContext());
        realm = Realm.getDefaultInstance();


        if(realm.where(Score.class).findFirst() == null) {
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    Score score = realm.createObject(Score.class);
                    score.setValeur(0);

                }
            });
        } else {
            score = realm.where(Score.class).findFirst().getValeur();
        }


        // init max size of the cat sequence using the level
        switch (level) {
            case 1:
                sequenceCatMaxSize = 6;
                break;
            case 2:
                sequenceCatMaxSize = 9;
                break;
            case 3:
                sequenceCatMaxSize = 12;
                break;
            case 4:
                sequenceCatMaxSize = 16;
                break;
            default:
                sequenceCatMaxSize = 6;
                break;
        }

        initView();
    }

    @Override
    protected void onStart() {
        super.onStart();
        // On the activity's start, the player button are disabled to let the computer play
        disableAllButton();
        // Animation of the computer are played
        playAnimation();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

    }

    /**
     * Initialization of the cat images button with the view
     * Initialization of the pictures for the cat in the image button
     */
    private void initImageButtons() {
        // Init image button
        maineCoonImageButton = (ImageButton) findViewById(maineCoon.getImageButtonId());
        persanImageButton = (ImageButton) findViewById(persan.getImageButtonId());
        sphinxImageButton = (ImageButton) findViewById(sphinx.getImageButtonId());
        scottishImageButton = (ImageButton) findViewById(scottish.getImageButtonId());

        // Picture attribution of the cat for the image button
        maineCoonImageButton.setImageResource(maineCoon.getSmallPictureId());
        persanImageButton.setImageResource(persan.getSmallPictureId());
        sphinxImageButton.setImageResource(sphinx.getSmallPictureId());
        scottishImageButton.setImageResource(scottish.getSmallPictureId());
    }

    private void initView() {
        initImageButtons();
        updateListCat();
        initClickListenerButtons();
        initSequenceCatComputer();

    }

    /**
     * Initialisation of click listener for the different image button
     * For the player click  it animate the current cat, add it to the player's cat sequence and
     * check if the choice correspond to the computer template
     */
    private void initClickListenerButtons() {

        // init of the click listener persan image button
        persanImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                persan.animateCat(view.getContext(), persanImageButton);
                sequenceCatPlayer.add(persan);
                compareCatSequence(view.getContext());
            }
        });

        // init of the click listener maine coon image button
        maineCoonImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                maineCoon.animateCat(view.getContext(), maineCoonImageButton);
                sequenceCatPlayer.add(maineCoon);
                compareCatSequence(view.getContext());

            }
        });

        // init of the click listener sphinx image button
        sphinxImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                sphinx.animateCat(view.getContext(), sphinxImageButton);
                sequenceCatPlayer.add(sphinx);
                compareCatSequence(view.getContext());
            }
        });

        // init of the click listener of the scottish image button
        scottishImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                scottish.animateCat(view.getContext(), scottishImageButton);
                sequenceCatPlayer.add(scottish);
                compareCatSequence(view.getContext());
            }
        });


    }

    /**
     * Check if the player's cat sequence correspond to the computer's cat sequence
     * This function defines also the score that the player will
     * @param context
     */
    public void compareCatSequence(Context context) {
        for (int i = 0; i < sequenceCatPlayer.size() && i < sequenceCatComputer.size(); i++) {
            // The player lose
            if (!sequenceCatPlayer.get(i).equals(sequenceCatComputer.get(i))) {
                Toast.makeText(context, "YOU LOSE", Toast.LENGTH_LONG).show();
                sequenceCatPlayer.get(i).cancelSound(context);
                disableAllButton();
                sequenceCatPlayer.get(i).setSoundId(R.raw.lose);
                sequenceCatPlayer.get(i).playSound(context);
                break;
            }
        }
        // The player win, its score is incremented the game redirect to the main menu
        if (sequenceCatPlayer.equals(sequenceCatComputer)) {

            Toast.makeText(context, "YOU WIN", Toast.LENGTH_LONG).show();
            disableAllButton();
            sequenceCatPlayer.get(sequenceCatPlayer.size() - 1).cancelSound(context);
            sequenceCatPlayer.get(sequenceCatPlayer.size() - 1).setSoundId(R.raw.win);
            sequenceCatPlayer.get(sequenceCatPlayer.size() - 1).playSound(context);
            ++score;
           // Update the score
            /*SharedPreferences sharedPref = this.getSharedPreferences("myprefrences", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putInt("score", score);
            editor.apply();*/
            final Score scoreObj = realm.where(Score.class).findFirst();
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    scoreObj.setValeur(score);
                }
            });


            Intent intent = new Intent(GameActivity.this, MainActivity.class);
            intent.putExtra("score", score);
            startActivity(intent);


        }
    }

    // Create the random cat sequence of the computer
    private void initSequenceCatComputer() {
        int minimum = 0;
        int maximum = catList.size() - 1;
        for(int i = 0; i < sequenceCatMaxSize; ++i){
            // The random is slightly modified to avoid the same sequence of 1 cat only
            if(i == 3 % 2){
                sequenceCatComputer.add(catList.getCatList().get(3));
            } else if (i == 2 % 3){
                sequenceCatComputer.add(catList.getCatList().get(2));
            }
            else {
                int randomNum = minimum + (int)(Math.random() * maximum);
                sequenceCatComputer.add(catList.getCatList().get(randomNum));
            }
        }
    }

    /**
     * This function will enable the cat's image button to able the player to play
     */
    private void enableAllButton() {
        scottishImageButton.setEnabled(true);
        sphinxImageButton.setEnabled(true);
        maineCoonImageButton.setEnabled(true);
        persanImageButton.setEnabled(true);
    }

    /**
     * This function will disable the cat's image button to able the computer to play without being disturbed by the player
     */
    private void disableAllButton() {
        scottishImageButton.setEnabled(false);
        sphinxImageButton.setEnabled(false);
        maineCoonImageButton.setEnabled(false);
        persanImageButton.setEnabled(false);
    }

    /**
     * This function will animate each button after it's clicked
     */
    private void playAnimation() {
        for (int i = 0; i < sequenceCatComputer.size(); i++) {

            final int finalI = i;
            final Context context = this;
            new Handler().postDelayed(new Runnable()
            {
                @Override
                public void run()
                {
                    disableAllButton();
                    sequenceCatComputer.get(finalI).animateCat(context,
                            (ImageButton)findViewById(sequenceCatComputer.get(finalI).getImageButtonId()));
                    if(finalI == sequenceCatComputer.size() - 1){
                        enableAllButton();
                    }
                }
            }, 1000 * i);
            if(i == sequenceCatComputer.size() - 1){
                enableAllButton();
            }
        }

    }

    /**
     * The list of cat is updated with the last version of cat initialised during the game
     * To avoid inconsistencies and problems
     */
    private void updateListCat() {
        List<Cat> tempListCat = new ArrayList<>();
        tempListCat.add(persan);
        tempListCat.add(scottish);
        tempListCat.add(sphinx);
        tempListCat.add(maineCoon);
        catList.setCatList(tempListCat);
    }



    @Override
    protected void onPause () {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close();
    }
}
